
def divisivel_por_tres(arr)
   
    #cria uma lista para receber os valores divisiveis por 3
    lista = []

    #percorre cada valor do array
    arr.each do |valor|
        #se valor tem resto 0 quando divisivel por 3 adciona na lista
        if valor%3==0
            lista.append(valor)
        end
    end
    print lista
end

array = [3,5,6]
divisivel_por_tres(array)
