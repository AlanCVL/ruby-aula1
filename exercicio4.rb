def cast_to_string(arr)
   
    #cria uma lista para receber os valores convertidos para string
    novaLista = []

    #percorre cada valor do array e converte-os em  string, adicionando em novaLista
    arr.each do |valor|
        novaLista.append(valor.to_s)
    end
    print novaLista
    
end

array = [25, 35, 45]
cast_to_string(array)