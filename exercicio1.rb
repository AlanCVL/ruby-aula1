valor = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
def soma_multiplica_arr(arr)
    #cria variaveis para receber os valores quando for para somar e multiplicar
    somador = 0
    multiplicador = 1

    #percorre a lista de listas
    arr.each do |valor|
        #entra em cada lista percorrendo-as e somando/multiplicando os valores 
        for i in valor
            somador +=i
            multiplicador *=i
        end
    end
    print  'Somador:', somador
    puts
    print 'Multiplicador', multiplicador
end
soma_multiplica_arr(valor)